ceph-ansible
============
Ansible playbooks for Ceph, the distributed filesystem.
Version : 4.0
For more infos: https://docs.ceph.com/projects/ceph-ansible/en/stable-4.0/

Installation guide:
1. Prepare all Nodes – ceph-ansible, OSD, MON, MGR, MDS
   a. Set Correct hostname on each server
   b. Set correct time and configure chrony NTP service
   c. Add hostname with IP addresses to DNS server or update /etc/hosts on all servers

Once you’ve done above tasks, install basic packages on each node:
   a. sudo dnf update
   b. sudo dnf install vim bash-completion tmux
   c. sudo dnf -y update && sudo reboot

2. Prepare Ceph Admin Node

   a. sudo dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm (For CentOS 7)
   a. sudo dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm (For CentOS 8)
   c. sudo dnf config-manager --set-enabled PowerTools
   d. sudo yum install git vim bash-completion

3. Clone this repo
   a. git clone https://foxlab.veolia-es.net/pit-open/ceph-ansible.git

4. Install Python3 and required dependencies
   a. sudo yum install python3-pip
   b. sudo pip3 install -r requirements.txt

5. Ensure /usr/local/bin path is added to PATH.
   a. echo "PATH=\$PATH:/usr/local/bin" >>~/.bashrc
   b. source ~/.bashrc

6. Copy SSH Public Key of ceph admin to all nodes
e.g:
for host in cephmon01 cephmon02 cephmon03 cephosd01 cephosd02 cephosd03; do
 ssh-copy-id root@$host
done

7. Edit files
   a. cd ceph-ansible
   b. The configuration file of your ceph cluster : group_vars/all.yml
   c. The configuration file to set osd devices : group_vars/osds.yml
   d. The hosts file : hosts

8. Deployment
Run this command : 
   a. ansible-playbook -i hosts site.yml --user=<user> --become --become-user=root -kK
When prompted specify your pwd and everithing should be okay :)

If you have any problem during installation feel free to contact ibrahima.diop.ext@veolia.com
